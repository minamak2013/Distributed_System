import java.util.HashMap;

public class CreateGraph extends SharedParameters 
{

	
	public static void add(int node1, int node2)
	{
		
		if (vertices == null) 
			vertices = new HashMap<>();
		
		
		boolean isNode1 = vertices.containsKey(node1);
		boolean isNode2 = vertices.containsKey(node2);

		
		if (!isNode1 && !isNode2) {
			Vertex v1 = new Vertex(node1);
			Vertex v2 = new Vertex(node2);
			
			v1.neighbours.put(node2, v2);
						
			vertices.put(node1, v1);
			vertices.put(node2, v2);
		
		}else if (isNode1 && !isNode2) 
		{
			Vertex v1 = vertices.get(node1);
			Vertex v2 = new Vertex(node2);
			
			v1.neighbours.put(node2, v2);
			
			vertices.put(node2, v2);
			
		}else if (!isNode1 && isNode2)
		{
			Vertex v1 = new Vertex(node1);
			Vertex v2 = vertices.get(node2);
			
			v1.neighbours.put(node2, v2);
			
			vertices.put(node1, v1);
			
		}else{
			Vertex v1 = vertices.get(node1);
			Vertex v2 = vertices.get(node2);
			
			HashMap<Integer, Vertex> neighbours = v1.neighbours;
			neighbours.put(node2, v2);
		}
		
		
	}
	
	
	public static void delete(int node1, int node2)
	{
		if (vertices.containsKey(node1) && vertices.containsKey(node2)) 
		{
			Vertex v1 = vertices.get(node1);		
			v1.neighbours.remove(node2);

		}
	}
	
	
}
