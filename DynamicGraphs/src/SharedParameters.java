import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

abstract class SharedParameters
{
	
	static HashMap<Integer, Vertex> vertices;
	
	static final ReentrantReadWriteLock acquireLockParsing = new ReentrantReadWriteLock(true);
	static final Lock readLockParsing = acquireLockParsing.readLock();
	static final Lock writeLockParsing = acquireLockParsing.writeLock();

}
