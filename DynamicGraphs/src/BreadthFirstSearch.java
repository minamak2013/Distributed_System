import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public class BreadthFirstSearch extends SharedParameters implements Callable<Integer>
{
		
  Queue<Integer> queue;
  Queue<Integer> visited;
  
  HashMap<Integer, Integer> distances;	// Hashmap<node id, distance> to store distances of each node
  
  ArrayList<Vertex> neighboursList;
  
  int source ;
  int destination;

  ExecutorService exec;
  List<Future<Integer>> results;

  int finalDistance;
  
  ReentrantReadWriteLock acquireLock = new ReentrantReadWriteLock(true);
  Lock writeLock = acquireLock.writeLock();

  
  
  public BreadthFirstSearch(int source, int destination)
  {
	  queue = new PriorityQueue<>();
	  visited = new PriorityQueue<>();
	  distances = new HashMap<>();
	  this.source = source;
	  this.destination = destination;
	  
	  exec = Executors.newFixedThreadPool(4);
      results = new ArrayList<>();
	
  }	

  
	
  public int breadthFirstSearchAlgorithm()
  {
	 queue = new PriorityQueue<>();
	 visited = new PriorityQueue<>();
	 distances = new HashMap<>();
	 
	 distances.put(source, 0);
	 
	 queue.offer(source);
     visited.offer(source);
     

      
     while (!queue.isEmpty())
     {
        int vertexID = queue.poll();  
        visited.offer(vertexID);
        
        //System.out.println("vertexID: " + vertexID);
        neighboursList = new ArrayList<>();

        neighboursList = getNeighbours(vertexID);
       
        
        if(neighboursList.size() < 50000)
        {
        	IterateOverNeighbours(vertexID);
        	
        }else if(neighboursList.size() < 500000)
        {
        	System.out.println("size: "+neighboursList.size());
        	results.add(exec.submit(new Iterate(0,vertexID, 2) ));
    		results.add(exec.submit(new Iterate(neighboursList.size()/2, vertexID, 2) ));
    		
    		try {
				results.get(0).get();
				results.get(1).get();
				
				results.clear();
			
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        	
        }else
        {
           	results.add(exec.submit(new Iterate(0, vertexID, 4) ));
    		results.add(exec.submit(new Iterate(neighboursList.size()/4, vertexID, 4) ));
    		results.add(exec.submit(new Iterate(neighboursList.size()/2, vertexID, 4) ));
    		results.add(exec.submit(new Iterate(neighboursList.size()* 3/4, vertexID, 4) ));
    		
    		try {
				results.get(0).get();
				results.get(1).get();
				results.get(2).get();
				results.get(3).get();

				results.clear();
			
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        }
        
              
     }
     
     	if(distances.get(destination) == null && source != destination){
     		return -1;
     	}
     		
     	return distances.get(destination);

    }
    
	
  private ArrayList<Vertex> getNeighbours(int node) 
  {
	 ArrayList<Vertex> neighboursList = new ArrayList<Vertex>();
	// System.out.println("vertices.get(node): " + vertices.get(node)+ " node: " + node);
	 Set<Integer> keys = vertices.get(node).neighbours.keySet();
	    
	 for (Integer key : keys)
	 {
	     neighboursList.add(vertices.get(node).neighbours.get(key));

	 }
     return neighboursList;
    
  }
  
  public void IterateOverNeighbours(int vertexID)
  {
	  for (Vertex neighbour :neighboursList)
      {   
      	if (!visited.contains(neighbour.id))
      	{
            distances.put(neighbour.id, distances.get(vertexID) + 1);
     
      		queue.offer(neighbour.id);
      		visited.offer(neighbour.id);
     		
      	}
      }
	  
	  
  }
  
  
  
  @Override
  public Integer call() throws Exception
  {
	  //System.out.println("Dest: " + destination);

	  readLockParsing.lock();
//	  System.out.println("Current Thread: " + Thread.currentThread().getName());
	  finalDistance = breadthFirstSearchAlgorithm();
	  readLockParsing.unlock();

	  return finalDistance;
	  
  }



  
	
	


private class Iterate implements Callable<Integer> 
{
	
	int begin;
	int vID;
	int divisor;
	
	public Iterate(int begin, int vID, int divisor)
	{
		this.begin = begin;
		this.vID = vID;
		this.divisor = divisor;
	}

	@Override
	public Integer call() throws Exception
	{
		double size = neighboursList.size();

		for (double i = begin; i< size/divisor +begin; i++)
        {
			Vertex neighbour = neighboursList.get((int) i);
           System.out.println("vertexID: " + vID + ", neighbour: " + neighbour.id);
          
        	if (!visited.contains(neighbour.id))
        	{
       
                writeLock.lock();
                distances.put(neighbour.id, distances.get(vID) + 1);
        		queue.offer(neighbour.id);
        		visited.offer(neighbour.id);
        		writeLock.unlock();
        		        		
        		
        	}
        }
		
		return 0;
	}
	
}

  


}
