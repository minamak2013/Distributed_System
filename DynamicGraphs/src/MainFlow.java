import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MainFlow
{
	
	FileInputStream fis = null;
	
	InputStreamReader input;
	BufferedReader br;
	String data;

	boolean inputEdge;
	boolean inputQuery;
	
	final int THREADS_NUMBER = 10;
    ExecutorService exec;
    List<Callable<Integer>> callables;
    List<Future<Integer>> results;
    
    long start;
	int counter = 0;

    
    public MainFlow() throws FileNotFoundException
    {
		fis = new FileInputStream("C:\\Users\\Ramy Soliman\\Desktop\\test-harness\\init-file.txt");
//		fis = new FileInputStream("input.txt");
		
		input = new InputStreamReader(fis);
		br = new BufferedReader(input);

    	inputEdge = true;
    	inputQuery = false;
        	
    	exec = Executors.newFixedThreadPool(THREADS_NUMBER);
    	callables = new ArrayList<Callable<Integer>>();
    	results = new ArrayList<>();
    	
    	start = 0;

    }
    
    
    public void executeProgram() throws NumberFormatException, IOException, InterruptedException, ExecutionException
    {
		while ((data = br.readLine()) != null) 
		{
			String[] arr;
				
			if(!inputQuery)	
				arr = data.split("	");
			else
				arr = data.split(" ");

			
			if (inputEdge)
			{
				inputGraph(arr);
				

			} else if (inputQuery)
			{
										
				if (arr[0].equalsIgnoreCase("FF")) 
				{
					executeThreads();
						
					long end = System.currentTimeMillis();
					System.out.println("Running Time : " + (end - start)+" msec");
					System.out.println("counter: " + counter);
						
					System.exit(0);
						
				}else if (arr[0].equalsIgnoreCase("F")) 
				{
					executeThreads();
						
					long end = System.currentTimeMillis();
					System.out.println("Running Time : " + (end - start)+" msec");
					start = System.currentTimeMillis();
								
				} else if (arr[0].equalsIgnoreCase("A") && arr.length >= 3)
				{
					executeThreads();
					CreateGraph.add(Integer.parseInt(arr[1]), Integer.parseInt(arr[2]));

				} else if (arr[0].equalsIgnoreCase("D") && arr.length >= 3)
				{
					executeThreads();		
					CreateGraph.delete(Integer.parseInt(arr[1]), Integer.parseInt(arr[2]));

				} else if (arr[0].equalsIgnoreCase("Q") && arr.length >= 3) 
				{
					//callables.add(new BreadthFirstSearch(Integer.parseInt(arr[1]), Integer.parseInt(arr[2])) );
					counter++;
					results.add(exec.submit(new BreadthFirstSearch(Integer.parseInt(arr[1]), Integer.parseInt(arr[2])) ));
					
							
				}else
				{
					System.out.println("QUERY Error");
				}

			}
		}

    	
    }
    
    
    private void executeThreads() throws InterruptedException, ExecutionException
    {
    	if(results.size() != 0)
    	{
    		//results = exec.invokeAll(callables);
    		
    		for(Future<Integer> result: results) 
    			System.out.println("Distances: " + result.get());
		 		         
    		
    		results.clear();
    		//callables.clear();
		    
    	}
    	
    }


    private void inputGraph(String arr[])
    {
    	if (arr[0].equalsIgnoreCase("S")) 
		{
			inputEdge = false;
			inputQuery = true;
		
		} else if (arr.length >= 2) 
		{
			CreateGraph.add(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]));

		}else 
		{
			System.out.println("Error in Graph Input");
		}
			
			start = System.currentTimeMillis();
    }
    
    
    
}
